package main

import (
	"CloudAssignment3/internal"
	"fmt"
	"log"
	"net/http"
)

//PORT for server to listen on.
const PORT = ":8080"


func main(){
	//commented out cause it needs database connection
	internal.DB.SetUpDatabase()
	internal.Timer()

	http.HandleFunc("/", internal.HomePageHandler)
	http.HandleFunc("/weather/v1/ip", internal.IPHandler)
	http.HandleFunc("/weather/v1/weather/", internal.WeatherHandler)
	http.HandleFunc("/weather/v1/country/", internal.CountryHandler)
	http.HandleFunc("/weather/v1/status", internal.StatusHandler)
	http.HandleFunc("/weather/v1/webhooks/", internal.WebhooksHandler)
	http.HandleFunc("/weather/v1/webhooks", internal.WebhooksHandler)

	fmt.Println("Listening on port " + PORT)
	listenError := http.ListenAndServe(PORT, nil)
	if listenError != nil{
		log.Fatal("Error while listening and serving on port " + PORT + "\n")
	}
}