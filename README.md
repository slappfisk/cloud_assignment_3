# Cloud_Assignment_3
By:

Mikkel Andreas Kvande

Per Ivar Kraggerud

Øyvind Stendal

Michael Eyob

Jonas Melby



### Documentation can be found on the [Wiki:](https://bitbucket.org/slappfisk/cloud_assignment_3/wiki/Home)


# How to use the application:
1. You can get the weather status for a given period, and optionally for a given city using:
http://10.212.137.165:8080/weather/v1/weather/{current/forecast/history}/{city}
Current: Displays the current weather.
Foreast: Displays the weather for each of the following 16 days.
History: Displays the weather for each hour for the last 24 hours.
Optionally, enter a city name in the last part of the URL. By default, this location is based on your IP address.

2. You can get countrystatus with current weather by using:
http://10.212.137.165:8080/weather/v1/country/{country_name}
country_name is optional, and if not used, the URL vil show to all countries in the world, with their capital.

3. You can get current location based on your IP by using:
http://10.212.137.165:8080/weather/v1/ip

4. To display stored webhook use:

- http://10.212.137.165:8080/weather/v1/webhooks/
  Displays all registered webhooks. If there are no registered webhooks, it displays null.
- http://10.212.137.165:8080/weather/v1/webhooks/{WebhooksId}
  WebhooksId is the int to specify what exact webhook you want to show, based on their Id.
  This is optional, if not used the server wil show all webhooks.

5. To delete one of the webhooks use:
http://10.212.137.165:8080/weather/v1/webhooks/{webhook_id} as an DELETE request with JUST the ID number of the webhook you want to delete as payload.

6. To store your own webhook use:
http://10.212.137.165:8080/weather/v1/webhooks as an POST request with wanted payload
This is how you payload should look like:
```
{
  "event": "commits|languages|status",
  "url": "Your Webhook URL"
}
```

To display status and information about server connection to:

https://ipstack.com/

https://openweathermap.org/

https://restcountries.eu/

MongoDB Database

Use: http://10.212.137.165:8080/weather/v1/status


# Testing
How to run the tests (in linux).
To run the tests you need to have mongodb installed in your machine. We have used mongodb on port 27018, and not the default port
27017, as this was often already in use. 

To run the tests:

   1. Run the following command on your linux terminal:     mongod --port 27018 
   
   2. Run the tests on your editor :                        go test -cover
