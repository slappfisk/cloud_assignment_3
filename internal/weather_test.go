package internal

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

var WeatherURL = "/weather/v1/weather/"


// Function that starts the request and returns the response recorder
func weatherRequest(method string, URL string, t *testing.T) httptest.ResponseRecorder{

	resp, err := http.NewRequest(method, URL, nil)

	if err != nil{
		t.Errorf("error making the %s request %s ", method, err)
	}
	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(WeatherHandler)
	handler.ServeHTTP(responseRecorder, resp)

	return *responseRecorder
}

// Tests valid endpoint urls
func TestWeatherHandler_URLPattern(t *testing.T) {

	responseRecorder := weatherRequest(GET, WeatherURL + "current", t)
	status := responseRecorder.Code

	if status != http.StatusOK {
		t.Errorf("expected %v got %v", http.StatusOK, status)
	}

	responseRecorder = weatherRequest(GET, WeatherURL + "forecast", t)
	status = responseRecorder.Code

	if status != http.StatusOK {
		t.Errorf("expected %v got %v", http.StatusOK, status)
	}

	responseRecorder = weatherRequest(GET, WeatherURL + "history", t)
	status = responseRecorder.Code

	if status != http.StatusOK {
		t.Errorf("expected %v got %v", http.StatusOK, status)
	}

	responseRecorder = weatherRequest(GET, WeatherURL + "history/oslo", t)
	status = responseRecorder.Code

	if status != http.StatusOK {
		t.Errorf("expected %v got %v", http.StatusOK, status)
	}

}


//Tests malformed urls
func TestWeatherHandler_MalformedURL(t *testing.T) {
	responseRecorder := weatherRequest(GET,"/weather/v1/weather/forecastt", t)
	status := responseRecorder.Code

	if status != http.StatusBadRequest {
		t.Errorf("expected %v got %v", http.StatusBadRequest, status)
	}

	responseRecorder = weatherRequest(GET,"/weather/v1/weather/current/gjøvik", t) //not capital city
	status = responseRecorder.Code

	if status == http.StatusOK {
		t.Errorf("expected %v got %v", http.StatusBadRequest, status)
	}

}
