package internal

import (
	"net/http"
	"strings"
)

//STATUSCOUNTRY is the rest countries api url
const STATUSCOUNTRY = "https://restcountries.eu/rest/v2/"
//STATUSIPSTACK is the url to get the ip address
const STATUSIPSTACK = "http://api.ipstack.com/check?access_key=93755af5b75caddef628dfd2688e8ef5"
//STATUSWEATHERMAP is the url for the weather forecast
const STATUSWEATHERMAP = "https://samples.openweathermap.org/data/2.5/forecast?id=524901&appid=b1b15e88fa797225412429c1c50c122a1"
var status Status

//StatusHandler shows the status of the services that the application depends on.
func StatusHandler(w http.ResponseWriter, r*http.Request) {

	http.Header.Add(w.Header(), "Content-type", "application/json")
	//Splits URL into different parts with "/" as seperator
	URLParts := strings.Split(r.URL.Path, "/")
	//Set version to be what comes from the URL.
	status.Version = URLParts[2]

	responseCountry, err := http.Get(STATUSCOUNTRY)
	if err != nil {
		CheckError(err, w)
	}
	//Set RestCountries status to be equal to statusCode from the response
	status.RestCountries = responseCountry.StatusCode

	responseIPStack, err := http.Get(STATUSIPSTACK)
	if err != nil {
		CheckError(err, w)
	}
	//Set IpStack status to be equal to statusCode from the response
	status.IPStack = responseIPStack.StatusCode

	responseWeatherMap, err := http.Get(STATUSWEATHERMAP)
	if err != nil {
		CheckError(err, w)
	}
	//Set WeatherMap status to be equal to statusCode from the response.
	status.WeatherMap = responseWeatherMap.StatusCode
	status.Uptime = UpTime()

	_, ok := DB.Init()

	if ok {
		status.Database = http.StatusOK
	}else{
		status.Database = http.StatusInternalServerError
	}

	Encode(w, status)

	sendToWebhooks(w, "Server status", "status")
}
