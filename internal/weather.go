package internal

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"
)

//WeatherbitURLCurrent is the url of the current weather.
var WeatherbitURLCurrent = "https://api.weatherbit.io/v2.0/current"
//WeatherbitURLForecast is the url of the weather forecast.
var WeatherbitURLForecast = "https://api.weatherbit.io/v2.0/forecast/daily"
//WeatherbitURLHistory is the url of the weather history.
var WeatherbitURLHistory = "https://api.weatherbit.io/v2.0/history/hourly"
//WeatherbitKey is the key for the weatherbit api.
var WeatherbitKey = "&key=73b920b3b2be45be9c2d89cafe96d825"
//WeatherDescCurrent contains the weather information for the current day
var WeatherDescCurrent = &WeatherDataCurrent{}
//WeatherDescForecast contains the weather information for the coming 16 days
var WeatherDescForecast = &WeatherDataForecast{}
// WeatherDescHistory contains the weather history of a city
var WeatherDescHistory =&WeatherDataHistory{}

//WeatherHandler shows the weather of a city. Depending on user input, it shows
// the current weather, weather forecast or weather history of the city.
func WeatherHandler(w http.ResponseWriter, r*http.Request) {
	// Splits URL into parts separated by a forward slash
	URLParts := strings.Split(r.URL.Path, "/")

	if len(URLParts) >= 5 && len(URLParts) <= 6 && (URLParts[4] == "current" || URLParts[4] == "forecast" || URLParts[4] == "history") {

		//tells the content type to be type json
		http.Header.Add(w.Header(), "Content-type", "application/json")

		// Obtains the users city based on his/her IP address
		DecodeCityFromIP(w)

		// Uses the time library to get the current and yesterdays date
		var CurrentTime = time.Now()
		CurrentDate := CurrentTime.Format("2006-01-02")
		CurrentTime = time.Now().AddDate(0, 0, -1)
		PrevDate := CurrentTime.Format("2006-01-02")


		// If the user has specified that he/she wants the weather forecast
		if URLParts[4] == "forecast" {
			// If the user has not specified a city, get the users location based on his/her IP address
			if len(URLParts) == 5 || (len(URLParts) == 6 && URLParts[5] == "") {

				// Gets weather information on the users current location
				response, err := http.Get(WeatherbitURLForecast + "?city=" + IPAdress.City + WeatherbitKey)
				if err != nil {
					CheckError(err, w)
				}

				// Decodes the wanted weather information to our "weather data" struct
				err = json.NewDecoder(response.Body).Decode(WeatherDescForecast)
				if err != nil {
					CheckError(err, w)
				}

				// Displays weather information
				Encode(w, WeatherDescForecast)

				sendToWebhooks(w, "Forecast based on IP", "weather")

				// Else, the user has defined given a city name (hopefully) and the weather info about this city is given
			} else {
				cityName := upperCaseFirst(URLParts[5])

				response, err := http.Get(WeatherbitURLForecast + "?city=" + cityName + WeatherbitKey)
				if err != nil {
					CheckError(err, w)
				}

				// Decodes the wanted weather information to our "weather data" struct
				err = json.NewDecoder(response.Body).Decode(WeatherDescForecast)
				if err != nil {
					CheckError(err, w)
				}

				// Makes sure that the location name has been written correctly
				if WeatherDescForecast.CityName == cityName {
					// Displays weather information
					Encode(w, WeatherDescForecast)
				} else {
					http.Error(w, "Not a valid location", http.StatusBadRequest)
				}

				sendToWebhooks(w, "Forecast based on City", "weather")
			}

			// If the user has specified if he/she wants the current weather
		} else if URLParts[4] == "current" {
			// If the user has not specified a city, get the users location based on his/her IP address
			if len(URLParts) == 5 || (len(URLParts) == 6 && URLParts[5] == "") {
				// Gets weather information on the users current location
				response, err := http.Get(WeatherbitURLCurrent + "?city=" + IPAdress.City + WeatherbitKey)
				if err != nil {
					CheckError(err, w)
				}

				// Decodes the wanted weather information to our "weather data" struct
				err = json.NewDecoder(response.Body).Decode(WeatherDescCurrent)
				if err != nil {
					CheckError(err, w)
				}

				// Displays weather information
				Encode(w, WeatherDescCurrent)

				sendToWebhooks(w, "Current based on IP", "weather")

				// Else, the user has defined given a city name (hopefully) and the weather info about this city is given
			} else {
				cityName := upperCaseFirst(URLParts[5])

				// Gets weather information on the city the user has provided in the endpoint
				response, err := http.Get(WeatherbitURLCurrent + "?city=" + cityName + WeatherbitKey)
				if err != nil {
					CheckError(err, w)
				}

				// Decodes the wanted weather information to our "weather data" struct
				err = json.NewDecoder(response.Body).Decode(WeatherDescCurrent)
				if err != nil {
					CheckError(err, w)
				}
				// Makes sure that the location name has been written correctly
				if WeatherDescCurrent.Data[0].CityName == cityName {
					// Displays weather information
					Encode(w, WeatherDescCurrent)
				} else {
					http.Error(w, "Not a valid location", http.StatusBadRequest)
				}

				sendToWebhooks(w, "Current based on City", "weather")
			}

			// If the user has specified if he/she wants past weather
		} else if URLParts[4] == "history" {
			if len(URLParts) == 5 || (len(URLParts) == 6 && URLParts[5] == "") {
				// Gets weather information on the users current location
				response, err := http.Get(WeatherbitURLHistory + "?city=" + IPAdress.City + "&start_date=" + PrevDate + "&end_date=" + CurrentDate + WeatherbitKey)
				if err != nil {
					CheckError(err, w)
				}

				// Decodes the wanted weather information to our "weather data" struct
				err = json.NewDecoder(response.Body).Decode(WeatherDescHistory)
				if err != nil {
					CheckError(err, w)
				}

				// Displays weather information
				Encode(w, WeatherDescHistory)

				sendToWebhooks(w, "History based on IP", "weather")

				// Else, the user has defined given a city name (hopefully) and the weather info about this city is given
			} else {
				cityName := upperCaseFirst(URLParts[5])

				// Gets weather information on the city the user has provided in the endpoint
				response, err := http.Get(WeatherbitURLHistory + "?city=" + cityName + "&start_date=" + PrevDate + "&end_date=" + CurrentDate + WeatherbitKey)
				if err != nil {
					CheckError(err, w)
				}

				// Decodes the wanted weather information to our "weather data" struct
				err = json.NewDecoder(response.Body).Decode(WeatherDescHistory)
				if err != nil {
					CheckError(err, w)
				}

				// Makes sure that the location name has been written correctly
				if WeatherDescHistory.CityName == cityName {
					// Displays weather information
					Encode(w, WeatherDescHistory)
				} else {
					http.Error(w, "Not a valid location", http.StatusBadRequest)
				}

				sendToWebhooks(w, "History based on City", "weather")
			}

		} else {
			http.Error(w, "Malformed URL", http.StatusBadRequest)
		}
	} else {
		http.Error(w, "Malformed URL", http.StatusBadRequest)
	}
}