package internal

import (
	"fmt"
	"net/http"
)

//HomePageHandler displays information about how to use the application.
func HomePageHandler(writer http.ResponseWriter, r*http.Request){
	if r.URL.Path == "/" {
		bytesWritten, printError := fmt.Fprintf(writer, "What can you do on this page?\n\n"+
			"1. Get the weather status for a given period, and optionally for a given city:\n" +
			"/weather/v1/weather/{current/forecast/history}/{city}\n" +
			"\t1a. Current: Displays the current weather.\n"+
			"\t1b. Foreast: Displays the weather for each of the following 16 days.\n"+
			"\t1c. History: Displays the weather for each hour for the last 24 hours.\n" +
			"\tOptionally, enter a city name in the last part of the URL. By default, this location is based on your IP address.\n\n" +

			"2. Get countrystatus with current weather by using:\n" +
			"/weather/v1/country/{country_name}\n" +
			"country_name is optional, and if not used, the URL vil show to all countries in the world, with their capital.\n\n" +

			"3. Get current location based on your IP by using:\n" +
			"/weather/v1/ip\n\n" +

			"4. To display status and information about server connection to: \n" +
			"https://ipstack.com/\n" +
			"https://openweathermap.org/\n" +
			"https://restcountries.eu/\n"+
			"Use: /weather/v1/status\n\n"+

			"5a. To display stored webhook use:\n"+
			"{URL} + {NumberOfWebhooks}\n" +
			"NumberOfWebhooks is an optional int, if not used the server wil show all webhooks.\n\n"+
			"5b. To store your own webhook use:\n"+
			"{URL} as an POST request with wanted payload\n" +
			"See here for example of payload: {URL}\n\n"+
			"5c. To delete one of the webhooks use:\n"+
			"{URL} as an DELETE request with JUST the ID number of the webhook you want to delete as payload\n\n")

		CheckError(printError, writer)
		if bytesWritten == 0{
			fmt.Println(fmt.Errorf("no text was written to page"))
		}

	} else {http.Error(writer, "Malformed URL", http.StatusBadRequest)}
}
