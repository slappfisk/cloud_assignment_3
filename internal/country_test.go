package internal

import (

	"net/http"
	"net/http/httptest"
	"testing"
)

var CountryURL = "/weather/v1/country/"
var GET = "GET"

// Function that starts the request and returns the response recorder
func CountryRequest(method string, URL string, t *testing.T) httptest.ResponseRecorder{

	resp, err := http.NewRequest(method, URL, nil)

	if err != nil{
		t.Errorf("error making the %s request %s ", method, err)
	}
	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(CountryHandler)
	handler.ServeHTTP(responseRecorder, resp)

    return *responseRecorder
}

//Tests the allowed url patterns in the country handler
func TestCountryHandler_URLPattern(t *testing.T) {

	responseRecorder := CountryRequest(GET,CountryURL, t)
	status := responseRecorder.Code

	if status != http.StatusOK {
		t.Errorf("expected %v got %v", http.StatusOK, status)
	}

	responseRecorder = CountryRequest(GET, CountryURL + "norway", t)
	status = responseRecorder.Code

	if status != http.StatusOK {
		t.Errorf("expected %v got %v", http.StatusOK, status)
	}

}

//Test checks if allowed method is "GET", other methods should not be acceptable
func TestCountryHandler_Methods(t *testing.T) {

	responseRecorder := CountryRequest("POST",CountryURL, t)
	status := responseRecorder.Code

	if status != http.StatusBadRequest {
		t.Errorf("expected %v got %v", http.StatusBadRequest, status)
	}

	responseRecorder = CountryRequest("PUT",CountryURL, t)
	status = responseRecorder.Code

	if status != http.StatusBadRequest {
		t.Errorf("expected %v got %v", http.StatusBadRequest, status)
	}

	responseRecorder = CountryRequest("DELETE",CountryURL, t)
	status = responseRecorder.Code

	if status != http.StatusBadRequest {
		t.Errorf("expected %v got %v", http.StatusBadRequest, status)
	}

}

//Tests malformed urls
func TestCountryHandler_MalformedURL(t *testing.T) {
	responseRecorder := CountryRequest(GET,"/weather/country/", t)
	status := responseRecorder.Code

	if status != http.StatusBadRequest {
		t.Errorf("expected %v got %v", http.StatusBadRequest, status)
	}

	responseRecorder = CountryRequest(GET,"/weather/v1/country//", t)
	status = responseRecorder.Code

	if status != http.StatusBadRequest {
		t.Errorf("expected %v got %v", http.StatusBadRequest, status)
	}

}

