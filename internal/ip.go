package internal

import (
	"encoding/json"
	"fmt"
	"net/http"
)

//IpstackURL is the url that gives the information about a city based on the ip address.
var IpstackURL = "http://api.ipstack.com/check?access_key=93755af5b75caddef628dfd2688e8ef5"
//IPAdress is an IP struct var
var IPAdress = &IP{}

//IPHandler gets the city name from the ip adress.
func IPHandler(w http.ResponseWriter, r*http.Request) {
	//tells the content type to be type json
	http.Header.Add(w.Header(), "Content-type", "application/json")

	response, err := http.Get(IpstackURL) //retrieves info from specified page number
	if err != nil{
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	_ = json.NewDecoder(response.Body).Decode(IPAdress)

	err = json.NewEncoder(w).Encode(IPAdress)
	if err != nil {
		panic(err)
	}

	sendToWebhooks(w, "Current IP location", "ip")
}