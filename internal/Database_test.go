package internal

import (
	"gopkg.in/mgo.v2"
	"reflect"
	"testing"
)

func setupDB(t *testing.T) *MongoDB{
	db := MongoDB{
		DatabaseURL:    "localhost:27018",
		DatabaseName:   "testDB",
		CollectionName: "tests",
	}
	session, err := mgo.Dial(db.DatabaseURL)

	if err != nil{
		t.Error(err)
	}

	defer session.Close()

	return &db
}

func dropDB(t *testing.T, db *MongoDB){
	session, err := mgo.Dial(db.DatabaseURL)

	if err != nil{
		t.Error(err)
	}

	defer session.Close()

	err = session.DB(db.DatabaseName).DropDatabase()
	if err != nil{
		t.Error(err)
	}
}

func TestMongoDB_SetUpDatabase(t *testing.T) {
	db := MongoDB{}
	db.SetUpDatabase()

	if db.DatabaseURL != "localhost:27018" || db.CollectionName != "webhooks"{
		t.Error("test set up DB failed")
	}
}

func TestMongoDB_Init(t *testing.T) {
	db := setupDB(t)
	defer dropDB(t, db)

	db, ok := db.Init()
	if ok != true && ok != false || reflect.TypeOf(db)!= reflect.TypeOf(&MongoDB{}) {
		t.Error("test initialise failed")
	}
}

func TestMongoDB_Add(t *testing.T){
	db := setupDB(t)
	defer dropDB(t, db)

	db, ok := db.Init()
	if !ok {
		t.Error("couldn't initialise database")
	}
	wh := Webhooks{}
	wh.Event = "weather"
	wh.URL = "some url"
	count := db.Insert(wh)
	if count != 1{
		t.Error("error adding webhooks in the database")
	}
}

func TestMongoDB_Find(t *testing.T) {
	db := setupDB(t)
	defer dropDB(t, db)

	db, ok := db.Init()
	if !ok {
		t.Error("couldn't initialise database")
	}
	wh := Webhooks{}
	wh.Event = "weather"
	wh.URL = "some url"
	count := db.Insert(wh)
	if count != 1{
		t.Error("error adding webhooks in the database")
	}
	webhooks := db.Find(count)

	if(Webhooks{}) == webhooks{
		t.Error("test find webhook failed")
	}

	if webhooks.ID != count || webhooks.URL != wh.URL ||
		webhooks.Event != wh.Event {
		t.Error("test find webhook failed")
	}

	all := db.AllCollection()

	if len(all) != 1 || all[0].Event != wh.Event || all[0].URL != wh.URL{
		t.Error("test get all webhooks failed")
	}
}

func TestMongoDB_Delete(t *testing.T) {
	db := setupDB(t)
	defer dropDB(t, db)

	db, ok := db.Init()
	if !ok {
		t.Error("couldn't initialise database")
	}
	wh := Webhooks{}
	wh.Event = "weather"
	wh.URL = "some url"
	count := db.Insert(wh)
	if count != 1{  //count == id
		t.Error("error adding webhooks in the database")
	}

	ok = db.Delete(count)

	if ok && db.Find(count) != (Webhooks{}){
		t.Error("error deleting from database")
	}

}
