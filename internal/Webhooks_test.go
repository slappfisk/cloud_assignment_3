package internal

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

var webhookURL = "/weather/v1/webhooks"

// Function that starts the request and returns the response recorder
func WebhookRequest(method string, URL string, t *testing.T) httptest.ResponseRecorder{

	resp, err := http.NewRequest(method, URL, nil)

	if err != nil{
		t.Errorf("error making the %s request %s ", method, err)
	}
	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(WebhooksHandler)
	handler.ServeHTTP(responseRecorder, resp)

	return *responseRecorder
}

//Tests webhook endpoint
func TestWebhooksHandler(t *testing.T) {

	// Allowed endpoints url
	responseRecorder := WebhookRequest(GET, webhookURL , t)
	status := responseRecorder.Code

	if status != http.StatusOK {
		t.Errorf("expected %v got %v", http.StatusOK, status)
	}

	responseRecorder = WebhookRequest(GET, webhookURL +"/" , t)
	status = responseRecorder.Code

	if status != http.StatusOK {
		t.Errorf("expected %v got %v", http.StatusOK, status)
	}


	//Malformed urls
	responseRecorder = WebhookRequest("POST", "/weather/v1/webhookss" , t)
	status = responseRecorder.Code

	if status != http.StatusBadRequest {
		t.Errorf("expected %v got %v", http.StatusBadRequest, status)
	}


	//Methods
	responseRecorder = WebhookRequest("DELETE", webhookURL +"/" , t)
	status = responseRecorder.Code

	if status == http.StatusOK { // delete without id
		t.Errorf("expected %v got %v", http.StatusBadRequest, status)
	}

	responseRecorder = WebhookRequest("PUT", webhookURL , t)
	status = responseRecorder.Code

	if status != http.StatusBadRequest{
		t.Errorf("expected %v got %v", http.StatusBadRequest, status)
	}

}
