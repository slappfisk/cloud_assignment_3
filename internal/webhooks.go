package internal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"
)

//CONTENTTYPE const
const CONTENTTYPE = "application/json"

/*
WebhooksHandler can do three things:
Post request, a new webhook from the user is created, stores it in an array.
Get request for all webhooks or one specific from the user.
Delete request, can delete one specific webhook based on ID from user.

 */
func WebhooksHandler(writer http.ResponseWriter, reader*http.Request) {
	http.Header.Add(writer.Header(), "content-type", CONTENTTYPE)

	switch reader.Method {

	//Reads information about a webhook for user, andre stores it in an array
	case http.MethodPost:
		if reader.URL.Path == "/weather/v1/webhooks" || reader.URL.Path == "/weather/v1/webhooks/"{
			newWebhook := Webhooks{}
			decodeError := json.NewDecoder(reader.Body).Decode(&newWebhook)		//Decodes information from user, to a struct
			CheckError(decodeError, writer)										//Checks for errors

			DB, ok := DB.Init()
			if !ok {
				 http.Error(writer, "Cannot start database", http.StatusInternalServerError)
              return
			}

			id := DB.Insert(newWebhook)

			if id == -1 {
				http.Error(writer, "Something went wrong: " , http.StatusInternalServerError)
				return
			}

			bytesWritten, printError := fmt.Fprintln(writer, id)		//Returns the ID of the wehook to the user

			CheckError(printError, writer)												//Checks for errors
			if bytesWritten == 0 {														//Checks if no bytes was written, then somwthing went wrong
				fmt.Println(fmt.Errorf("no text was written to page"))
			}

		} else { http.Error(writer, "Malformed URL", http.StatusBadRequest) }


	//GET request to print out webhooks
	case http.MethodGet:
		URLParts := strings.Split(reader.URL.Path, "/")
		if reader.URL.Path == "/weather/v1/webhooks/" {	//If user wants all webhooks

			DB, ok := DB.Init()
			if !ok {
				http.Error(writer, "Cannot start database", http.StatusInternalServerError)
				return
			}
			allWebhooks := DB.AllCollection()

			encodeError := json.NewEncoder(writer).Encode(&allWebhooks)		//Encode all webhooks
			CheckError(encodeError, writer)									//And check for error
			//If not, but user want one specific webhook
		} else if len(URLParts) == 5 && URLParts[1] == "weather" && URLParts[2] == "v1" && URLParts[3] == "webhooks"{

			webhookID, convertError := strconv.Atoi(URLParts[4]) //Get this ID to a string
			CheckError(convertError, writer)                     //Check for errors

			DB, ok := DB.Init()
			if !ok {
				http.Error(writer, "Cannot start database", http.StatusInternalServerError)
				return
			}
			webhook := DB.Find(webhookID)

			if (Webhooks{}) == webhook{
				http.Error(writer, "Couldn't find webhook on db database: " , http.StatusBadRequest)
				return
			}

			encodeError := json.NewEncoder(writer).Encode(&webhook) //Encode this to user
			CheckError(encodeError, writer)                               //And check for errors
		}

	//Deletes the webhook with the id sendt from user
	case http.MethodDelete:
		URLParts := strings.Split(reader.URL.Path, "/")
		if len(URLParts) == 5 && URLParts[1] == "weather" && URLParts[2] == "v1" && URLParts[3] == "webhooks"{

			webhookID, convertError := strconv.Atoi(URLParts[4])
			CheckError(convertError, writer)								//Checks for errors

			DB, ok := DB.Init()
			if !ok {
				http.Error(writer, "Cannot start database", http.StatusInternalServerError)
				return
			}
			deleted := DB.Delete(webhookID)

			if deleted == true {
				//Tells the user if the webhook was deleted
				bytesWritten, printError := fmt.Fprintln(writer, "Webhook with ID =", webhookID, "was deleted")

				CheckError(printError, writer)				//Checks for errors
				if bytesWritten == 0 {						//If nothing was written to user, then something went wrong
					fmt.Println(fmt.Errorf("no text was written to page"))
				}
				//
			} else {
				http.Error(writer, "Couldn't delete webhook", http.StatusInternalServerError)
				return
			}
		}

	default:		//Tells the user that this action is noe available
		http.Error(writer, "Invalid method "+reader.Method, http.StatusBadRequest)
	}
}


//This function sends an eventlog to all webhooks connected to the specified event.
func sendToWebhooks(writer http.ResponseWriter, params string, event string){

	DB, ok := DB.Init()
	if !ok {
		http.Error(writer, "Cannot start database", http.StatusInternalServerError)
		return
	}
	allWebhooks := DB.AllCollection()

	for _, object := range allWebhooks{										//For alle webhooks
		//if strings.ToUpper(object.Event) == event{   //If that webhook has the event from parameter
		  if object.Event == event {
			var invocation = Invocation{Event: object.Event, Params: params, TimeStamp: time.Now()}		//Creates the struct to send

			data, marshalError := json.Marshal(invocation)					//Marshals this to json format
			CheckError(marshalError, writer)								//Checks for errors

			//Sends a porst request with URL, Contenttype and the struct as a buffer
			_, postError := http.Post(object.URL, CONTENTTYPE, bytes.NewBuffer(data))
			CheckError(postError, writer)			//Checks for errors
		}
	}
}

