package internal

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

//DB is the database struct variable
var DB = &MongoDB{}

//SetUpDatabase sets the DB name, URL and collection name
func(db* MongoDB)SetUpDatabase() {
	*db = MongoDB{
		DatabaseURL:    "localhost:27018",
		DatabaseName:   "weatherDB",
		CollectionName: "webhooks",
	}
}

//Init initialises database
func (db *MongoDB)Init() (*MongoDB, bool){

	session, err := mgo.Dial(db.DatabaseURL)

	if err != nil{
		fmt.Println("Cant connect to database : "+ err.Error())
		return &MongoDB{}, false
	}

	defer session.Close()

	return db, true

}

//Insert inserts a new webhook in the database
func (db *MongoDB) Insert(wh Webhooks) int {

	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil{
		fmt.Println("Cant connect to database : "+ err.Error())
		return -1
	}

	defer session.Close()

	count, err := session.DB(db.DatabaseName).C(db.CollectionName).Count()

	if err != nil{
		return -1
	}

	count ++
	wh.ID = count
	wh.Timestamp = time.Now()

	err = session.DB(db.DatabaseName).C(db.CollectionName).Insert(wh)

	if err != nil{
		return - 1
	}

	return count  // the id for the webhook
}

// AllCollection returns all the webhooks in the database
func (db *MongoDB) AllCollection() []Webhooks{

	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil{
		fmt.Println("Cant connect to database : "+ err.Error())
		return [] Webhooks{}
	}
	var all [] Webhooks

	err = session.DB(db.DatabaseName).C(db.CollectionName).Find(bson.M{}).All(&all)

	if err != nil{
		return [] Webhooks{}
	}
	return all

}

// Find searches a single webhook from the database with
// a certain id and returns it
func (db *MongoDB) Find(i int) Webhooks{

	session, err := mgo.Dial(db.DatabaseURL)

	if err != nil{
		fmt.Println("Cant connect to database : "+ err.Error())
		return  Webhooks{}
	}
	defer session.Close()

	all := db.AllCollection()

	for j:= 0; j < len(all); j++{
		if all[j].ID == i{
			return all[j]
		}
	}

	return  Webhooks{}

}

// Delete deletes a webhook with a given id
func(db *MongoDB) Delete(id int) bool {

	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		fmt.Println("Can't connect to database : " + err.Error())
		return false
	}
	defer session.Close()

	all := db.AllCollection()

	for j := 0; j < len(all); j++ {
		if all[j].ID == id {

			err = session.DB(db.DatabaseName).C(db.CollectionName).Remove(bson.M{"_id": all[j].IDByson})

			if err != nil {
				fmt.Println("Can't delete from database : " + err.Error())
				return false
			}
		}
	}
	return true

}