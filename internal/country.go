package internal

import (
	"encoding/json"
	"net/http"
	"strings"
)

// COUNTRYURL is the url from rest countries api to search country by name.
const COUNTRYURL = "https://restcountries.eu/rest/v2/name/"
// ALLCOUNTRYURL is the url from rest countries api to search for all countries.
const ALLCOUNTRYURL = "https://restcountries.eu/rest/v2/all"

//CountryHandler returns 3 different outputs based on user parameters on the endpoint:
//   - if no country name and no capital name, returns info about all countries from rest countries api.
//   - if country name but no capital name, returns info about that country from rest countries api.
//   - if country and capital name are given, returns info about capital (current weather data, population,
//      timezone, latitude and longitude)
func CountryHandler(writer http.ResponseWriter, r*http.Request) {

	if r.Method == http.MethodGet {
		http.Header.Add(writer.Header(), "content-type", "application/json")

		//Splits the URL specified by the user, everywhere there is a "/".
		URLParts := strings.Split(r.URL.Path, "/")

			//If the URL is valid
		if len(URLParts) == 5 {

			//If no country name is given
			if URLParts[4] == "" {
					//Struct objects to store data in
				var simpleCountry = &[]SimpleCountry{}

				resp, err := http.Get(ALLCOUNTRYURL)
				CheckError(err, writer)

				err = json.NewDecoder(resp.Body).Decode(simpleCountry)
				CheckError(err, writer)

				err = resp.Body.Close()
				CheckError(err, writer)

					// return info about all countries from rest countries api.
				err = json.NewEncoder(writer).Encode(simpleCountry)
				CheckError(err, writer)

				sendToWebhooks(writer, "List of all countries with their capital", "country")

				//Then country name is given.
			} else {
					//Struct objects to store data in
				var country = &[]Country{}
				var cityWeather = &CityWeather{}
				var weather = &WeatherDataCurrent{}
				URL := COUNTRYURL + URLParts[4]

					//gets info about capital from rest countries api
				resp, err := http.Get(URL)
				CheckError(err, writer)

				if resp.StatusCode != 200 {
					http.Error(writer, "only countries  allowed", http.StatusBadRequest)
					return
				}

				err = json.NewDecoder(resp.Body).Decode(country)
				CheckError(err, writer)

				err = resp.Body.Close()
				CheckError(err, writer)

				weatherURL := WeatherbitURLCurrent + "?city=" + (*country)[0].Capital + WeatherbitKey

				//gets weather info about capital from weatherbit api.
				resp, err = http.Get(weatherURL)
				CheckError(err, writer)

				err = json.NewDecoder(resp.Body).Decode(weather)
				CheckError(err, writer)

				err = resp.Body.Close()
				CheckError(err, writer)

				// copy the capital info and the weather data in the Capital and WeatherDataCurrent structs
				// inside the CityWeather struct
				cityWeather.Info = (*country)[0]
				cityWeather.Weather = *weather
				err = json.NewEncoder(writer).Encode(cityWeather)
				CheckError(err, writer)

				sendToWebhooks(writer, "Country information with weather", "country")
			}
		} else {
			http.Error(writer, "Malformed url", http.StatusBadRequest)
		}
	} else { // if method used is not GET
		http.Error(writer, "Method not allowed", http.StatusBadRequest)
	}
}