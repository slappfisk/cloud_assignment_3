package internal

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

//IP contains the current location of the user based on his/her IP address
type IP struct{
	City string `json:"city"`
}

//WeatherDataCurrent contains the weather information for the current day
type WeatherDataCurrent struct{
	Data[] struct{
		CityName string `json:"city_name"`
		Weather struct{
			Description string `json:"description"`
		}
		Temperature float32 `json:"temp"`
		ApparentTemp float32 `json:"app_temp"`
		WindSpeed float32 `json:"wind_spd"`
		Clouds int `json:"clouds"`
		Sunrise string `json:"sunrise"`
		Sunset string `json:"sunset"`
	} `json:"data"`
}

//WeatherDataForecast contains the weather information for the coming 16 days
type WeatherDataForecast struct{
	CityName string `json:"city_name"`
	Data[] struct{
		Weather struct{
			Description string `json:"description"`
		}
		Date string `json:"valid_date"`
		Temp float32 `json:"temp"`
		WindSpeed float32 `json:"wind_spd"`
		Clouds int `json:"clouds"`
	} `json:"data"`
}

// WeatherDataHistory contains the weather information for each of the next 24 hours
type WeatherDataHistory struct{
	CityName string `json:"city_name"`
	Data[] struct{
		Date string `json:"timestamp_local"`
		Temp float32 `json:"temp"`
		WindSpeed float32 `json:"wind_spd"`
		Clouds int `json:"clouds"`
	} `json:"data"`
}

//Webhooks is the structure of a webhook.
// Contains the url to invoke upon triggering of an event
type Webhooks struct {
	IDByson     bson.ObjectId `bson:"_id,omitempty"`
	ID		    int
	Event		string			`json:"event"`
	URL			string			`json:"url"`
	Timestamp	time.Time
}

// MongoDB : info om Database URL, Name and collection
type MongoDB struct {
	DatabaseURL string
	DatabaseName string
	CollectionName string
}

// Invocation is the struct of the message to send to the webhooks.
type Invocation struct{
	Event	string	`json:"eventType"`		// event type as per specification for registration
	Params	string						// parameters passed along in triggering request
	TimeStamp time.Time						// timestamp of request
}


// CityWeather contains weather information about a city
type CityWeather struct{
	Info Country
    Weather WeatherDataCurrent
}


// Country is the struct with country info
type Country struct {
	Code string `json:"alpha2code"`
	ContryName string `json:"name"`
	Capital string `json:"capital"`
	Population int `json:"population"`
	Area float32 `json:"area"`
	Coordinates[] float32 `json:"latlng"`
	Timezones[] string `json:"timezones"`
}

//SimpleCountry is a struct to use when listing all countries in the world, with their capital.
type SimpleCountry struct {
	ContryName string `json:"name"`
	Capital string `json:"capital"`
}

//Status struct for the services that the application depends on
type Status struct {
	IPStack int
	WeatherMap int
	RestCountries int
	Database int
	Uptime int
	Version string
}



