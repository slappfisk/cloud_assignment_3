package internal

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"
)

var startTime time.Time


//Timer function starts a timer
func Timer() {
	startTime = time.Now()
}
//UpTime returns time in seconds since startTime started.
func UpTime() int {
	return int(time.Since(startTime))/1000000000
}
//CheckError writes out an Internal Server Error if the error parameter is not nil.
func CheckError(error error, writer http.ResponseWriter){
	if error != nil{
		http.Error(writer, error.Error(), http.StatusInternalServerError)
		return
	}
}

//DecodeCityFromIP decodes the city name to an "IP" struct
func DecodeCityFromIP(w http.ResponseWriter){
	response, err := http.Get(IpstackURL) //retrieves info from specified page number
	if err != nil {
		CheckError(err, w)
	}

	// Decodes location information based on the users IP address
	err = json.NewDecoder(response.Body).Decode(IPAdress)
	if err != nil {
		CheckError(err, w)
	}
}

//Encode an optional struct
func Encode(w http.ResponseWriter, Struct interface{}){
	err := json.NewEncoder(w).Encode(Struct)
	if err != nil {
		CheckError(err, w)
	}
}

func upperCaseFirst(city string) string {
	splits := strings.SplitAfter(city, "")
	splits[0] = strings.ToUpper(splits[0])
	newString := ""

	for i := range splits {
		newString = newString + splits[i]
	}
	return newString
}
